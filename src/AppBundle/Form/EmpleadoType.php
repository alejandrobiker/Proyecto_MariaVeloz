<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class EmpleadoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('ci', TextType::class, array(
                    'label' => 'Cedula',
                ))
          ->add('prnombre', TextType::class, array(
                    'label' => 'Primer Nombre',
                ))
          ->add('prapellido', TextType::class, array(
                    'label' => 'Segundo Nombre',
                ))
          ->add('sexo', ChoiceType::class, array(
                    'choices' => array('Masculino' => 'M', 'Femenino' => 'F'),
                    'required' => true,
                    'attr' => array('placeholder' => 'Genero'),
                    'label' => 'Genero',
                ))
          ->add('edocivil', ChoiceType::class, array(
                    'choices' => array('Soltero' => 'soltero', 'Casado' => 'casado', 'Viudo' => 'viudo', 'Divorsiado' => 'divorsiado', 'Concubino' => 'concubino'),
                    'required' => true,
                    'attr' => array('placeholder' => 'Estado Civil'),
                    'label' => 'Estado Civil',
                ))
          ->add('fecnac', BirthdayType::class, array(
                    'placeholder' => array(
                        'year' => 'Año', 'month' => 'Mes', 'day' => 'Día',
                    ),
                    'label' => 'Fecha de Nacimiento',
                ))
          ->add('canthijos', ChoiceType::class, array(
                    'choices' => array('1' => '1', '2' => '2', '3' => '3', '4 o mas' => '4 o mas' ),
                    'label' => 'Cantidad hijos',
                ))
          ->add('telefono')
          ->add('direccion')
          ->add('sueldo')
          ->add('codempresa')
          ->add('codcargo');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Empleado'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_empleado';
    }


}
