<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empleado
 *
 * @ORM\Table(name="empleado", indexes={@ORM\Index(name="codempresa", columns={"codempresa"}), @ORM\Index(name="codcargo", columns={"codcargo"})})
 * @ORM\Entity
 */
class Empleado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ci", type="integer", nullable=false)
     */
    private $ci;

    /**
     * @var string
     *
     * @ORM\Column(name="prnombre", type="string", length=20, nullable=false)
     */
    private $prnombre;

    /**
     * @var string
     *
     * @ORM\Column(name="prapellido", type="string", length=20, nullable=false)
     */
    private $prapellido;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=false)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="edocivil", type="string", length=1, nullable=false)
     */
    private $edocivil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecnac", type="date", nullable=false)
     */
    private $fecnac;

    /**
     * @var integer
     *
     * @ORM\Column(name="canthijos", type="integer", nullable=false)
     */
    private $canthijos;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=12, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=20, nullable=false)
     */
    private $direccion;

    /**
     * @var float
     *
     * @ORM\Column(name="sueldo", type="float", precision=10, scale=0, nullable=false)
     */
    private $sueldo;

    /**
     * @var \Empresa
     *
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codempresa", referencedColumnName="id")
     * })
     */
    private $codempresa;

    /**
     * @var \Cargo
     *
     * @ORM\ManyToOne(targetEntity="Cargo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codcargo", referencedColumnName="id")
     * })
     */
    private $codcargo;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ci
     *
     * @param integer $ci
     *
     * @return Empleado
     */
    public function setCi($ci)
    {
        $this->ci = $ci;

        return $this;
    }

    /**
     * Get ci
     *
     * @return integer
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * Set prnombre
     *
     * @param string $prnombre
     *
     * @return Empleado
     */
    public function setPrnombre($prnombre)
    {
        $this->prnombre = $prnombre;

        return $this;
    }

    /**
     * Get prnombre
     *
     * @return string
     */
    public function getPrnombre()
    {
        return $this->prnombre;
    }

    /**
     * Set prapellido
     *
     * @param string $prapellido
     *
     * @return Empleado
     */
    public function setPrapellido($prapellido)
    {
        $this->prapellido = $prapellido;

        return $this;
    }

    /**
     * Get prapellido
     *
     * @return string
     */
    public function getPrapellido()
    {
        return $this->prapellido;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     *
     * @return Empleado
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set edocivil
     *
     * @param string $edocivil
     *
     * @return Empleado
     */
    public function setEdocivil($edocivil)
    {
        $this->edocivil = $edocivil;

        return $this;
    }

    /**
     * Get edocivil
     *
     * @return string
     */
    public function getEdocivil()
    {
        return $this->edocivil;
    }

    /**
     * Set fecnac
     *
     * @param \DateTime $fecnac
     *
     * @return Empleado
     */
    public function setFecnac($fecnac)
    {
        $this->fecnac = $fecnac;

        return $this;
    }

    /**
     * Get fecnac
     *
     * @return \DateTime
     */
    public function getFecnac()
    {
        return $this->fecnac;
    }

    /**
     * Set canthijos
     *
     * @param integer $canthijos
     *
     * @return Empleado
     */
    public function setCanthijos($canthijos)
    {
        $this->canthijos = $canthijos;

        return $this;
    }

    /**
     * Get canthijos
     *
     * @return integer
     */
    public function getCanthijos()
    {
        return $this->canthijos;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Empleado
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Empleado
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set sueldo
     *
     * @param float $sueldo
     *
     * @return Empleado
     */
    public function setSueldo($sueldo)
    {
        $this->sueldo = $sueldo;

        return $this;
    }

    /**
     * Get sueldo
     *
     * @return float
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }

    /**
     * Set codempresa
     *
     * @param \AppBundle\Entity\Empresa $codempresa
     *
     * @return Empleado
     */
    public function setCodempresa(\AppBundle\Entity\Empresa $codempresa = null)
    {
        $this->codempresa = $codempresa;

        return $this;
    }

    /**
     * Get codempresa
     *
     * @return \AppBundle\Entity\Empresa
     */
    public function getCodempresa()
    {
        return $this->codempresa;
    }

    /**
     * Set codcargo
     *
     * @param \AppBundle\Entity\Cargo $codcargo
     *
     * @return Empleado
     */
    public function setCodcargo(\AppBundle\Entity\Cargo $codcargo = null)
    {
        $this->codcargo = $codcargo;

        return $this;
    }

    /**
     * Get codcargo
     *
     * @return \AppBundle\Entity\Cargo
     */
    public function getCodcargo()
    {
        return $this->codcargo;
    }
  
   
}
